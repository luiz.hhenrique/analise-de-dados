package br.com.south.shared;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public abstract class File extends DataMachine {

	protected java.io.File getFileByHomePath(String complement) {
		return new java.io.File(this.getHomePath() + complement);
	}

	protected List<String> readFile(String filePath, String filename) {
		log.info("Lendo arquivo(s).");

		Path path = Paths.get(filePath + this.getDriver() + filename);
		List<String> lines = new ArrayList<>();
		try {
			lines = Files.lines(path, StandardCharsets.UTF_8).collect(Collectors.toList());
		}
		catch (IOException e) {
			log.error("Error message: {}", e.getMessage());
			log.error("Error stack: " + e);
		}
		return lines;
	}

	protected void writeFile(String filePath, String filename, byte[] bytes) {
		Path path = Paths.get(filePath + this.getDriver() + filename + "_" + new Date().getTime() + ".dat");
		try {
			Files.write(path, bytes, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW);
		}
		catch (IOException e) {
			log.error("Error message: {}", e.getMessage());
			log.error("Error stack> " + e);
		}
	}
}
