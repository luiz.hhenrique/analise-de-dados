package br.com.south.shared;

import br.com.south.service.ImporterService;
import br.com.south.service.impl.ImporterServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

@Slf4j
@Component
public class WatchFolder extends File {

	private final ImporterService importerService;

	@Autowired
	public WatchFolder(ImporterService importerService) {
		this.importerService = importerService;
	}

	public void watchFolderIN() {

		new Thread(() -> {

			String filePath = this.getFileByHomePath("/data/in").getPath();
			WatchService watchService;
			Path paths = Paths.get(filePath);
			WatchKey key;

			try {
				watchService = FileSystems.getDefault().newWatchService();
				paths.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
				while ((key = watchService.take()) != null) {
					log.info("Monitorando diretório.");
					for (WatchEvent<?> pollEvent : key.pollEvents()) {
						String fileName = String.valueOf(pollEvent.context());
						if (!fileName.contains(".dat")) {
							log.error("Arquivo incompatível, o(s) arquivo(s) devem ter o formato .dat");
							return;
						}
						List<String> lines = this.readFile(filePath, fileName);
						this.importerService.separateDataAndImportContent(lines);
					}
				}
				key.reset();
			}
			catch (IOException | InterruptedException e) {
				log.error("Error message: {}", e.getMessage());
				log.error("Error stack: " + e);
			}

		}).start();
	}
}
