package br.com.south.shared;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class DataMachine {

	protected String getOS() {
		return System.getProperty("os.name");
	}

	protected String getUsername() {
		return System.getProperty("user.name");
	}

	protected String getHomePath() {
		return System.getProperty("user.home");
	}

	protected String getDriver() {
		String osName = getOS();
		String driver = null;
		if (osName.indexOf("win") >= 0 || osName.indexOf("Windows 10") >= 0) {
			driver = "\\";
		}
		else if (osName.indexOf("mac") >= 0) {
			driver = "/";
		}
		else if (osName.indexOf("nux") >= 0) {
			driver = "/";
		}
		return driver;
	}
}
