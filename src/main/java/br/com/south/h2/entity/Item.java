package br.com.south.h2.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ITEM")
@Getter @Setter
public class Item implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true)
	private Long id;

	@Column(name = "ITEM_ID")
	private String itemId;

	@Column(name = "AMOUNT")
	private Long amount;

	@Column(name = "PRICE")
	private Double price;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "item")
	private Set<Sale> sales = new HashSet<>(0);
}
