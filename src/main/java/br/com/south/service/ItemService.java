package br.com.south.service;

import br.com.south.h2.entity.Item;

import javax.transaction.Transactional;
import java.util.List;

public interface ItemService {

	@Transactional(rollbackOn = Exception.class)
	Item saveOrUpdate(Item item);

	List<Item> findAll();
}
