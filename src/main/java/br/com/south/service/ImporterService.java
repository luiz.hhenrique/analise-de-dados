package br.com.south.service;

import java.util.List;

public interface ImporterService {

	void importArchiveDataInit();

	void separateDataAndImportContent(List<String> lines);
}
