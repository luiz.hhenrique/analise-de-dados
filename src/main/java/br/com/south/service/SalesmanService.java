package br.com.south.service;

import br.com.south.h2.entity.Salesman;

import javax.transaction.Transactional;
import java.util.List;

public interface SalesmanService {

	@Transactional(rollbackOn = Exception.class)
	Salesman saveOrUpdate(Salesman salesman);

	int totalSalesman();

	String worstSalesman();

	List<Salesman> findAll();
}
