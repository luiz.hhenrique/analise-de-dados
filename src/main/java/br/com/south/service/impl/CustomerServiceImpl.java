package br.com.south.service.impl;

import br.com.south.h2.entity.Customer;
import br.com.south.h2.repository.CustomerRepository;
import br.com.south.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository repository;

	@Autowired
	public CustomerServiceImpl(CustomerRepository repository) {
		this.repository = repository;
	}

	@Override
	public Customer saveOrUpdate(Customer customer) {
		log.info("Salvando cliente.");
		return repository.save(customer);
	}

	@Override
	public int totalCustomer() {
		log.info("Obtendo quantidade de clientes");
		return this.findAll().size();
	}

	@Override
	public List<Customer> findAll() {
		return repository.findAll();
	}
}
