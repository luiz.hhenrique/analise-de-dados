package br.com.south.service.impl;

import br.com.south.h2.entity.Customer;
import br.com.south.h2.entity.Item;
import br.com.south.h2.entity.Sale;
import br.com.south.h2.entity.Salesman;
import br.com.south.service.CustomerService;
import br.com.south.service.ExporterService;
import br.com.south.service.ImporterService;
import br.com.south.service.ItemService;
import br.com.south.service.SaleService;
import br.com.south.service.SalesmanService;
import br.com.south.shared.File;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class ImporterServiceImpl extends File implements ImporterService {

	private final SaleService saleService;
	private final SalesmanService salesmanService;
	private final CustomerService customerService;
	private final ItemService itemService;
	private final ExporterService exporterService;

	@Autowired
	public ImporterServiceImpl(SaleService saleService, SalesmanService salesmanService, CustomerService customerService, ItemService itemService, ExporterService exporterService) {
		this.saleService = saleService;
		this.salesmanService = salesmanService;
		this.customerService = customerService;
		this.itemService = itemService;
		this.exporterService = exporterService;
	}

	@Override
	public void importArchiveDataInit() {
		log.info("Obtendo arquivo de dados e iniciando importanção.");
		var path = this.getFileByHomePath("/data/in");
		for (String filename : Objects.requireNonNull(path.list())) {

			if (!filename.contains(".dat")) {
				log.error("Arquivo incompatível, o(s) arquivo(s) devem ter o formato .dat");
				return;
			}

			List<String> lines = this.readFile(path.getPath(), filename);
			this.separateDataAndImportContent(lines);
		}
	}

	@Override
	public void separateDataAndImportContent(List<String> lines) {
		log.info("Separando os dados e importando.");
		lines.forEach(line -> {
			List<String> contents = Arrays.asList(line.split("ç"));

			String id = contents.get(0);

			if (id.startsWith("001")) {
				Salesman salesman = new Salesman();
				salesman.setDocument(contents.get(1));
				salesman.setName(contents.get(2));
				salesman.setSalary(Double.valueOf(contents.get(3)));

				this.salesmanService.saveOrUpdate(salesman);
			}
			else if (id.startsWith("002")) {
				Customer customer = new Customer();
				customer.setDocument(contents.get(1));
				customer.setName(contents.get(2));
				customer.setBusinessArea(contents.get(3));

				this.customerService.saveOrUpdate(customer);
			}
			else if (id.startsWith("003")) {
				Sale sale = new Sale();
				sale.setSaleId(contents.get(1));

				List<String> itens = Arrays.asList(contents.get(2).replace("[", "").replace("]", "").split(","));
				Item item = new Item();
				item.setItemId(itens.get(0));

				String[] amounts = itens.get(1).split("-");
				item.setAmount(Double.valueOf(amounts[2]).longValue());

				String[] prices = itens.get(2).split("-");
				item.setPrice(Double.valueOf(prices[2]));
				item = this.itemService.saveOrUpdate(item);

				sale.setSalesman(contents.get(3));
				sale.setItem(item);

				this.saleService.saveOrUpdate(sale);
				this.exporterService.exportReport(this.exporterService.mountReport());
			}
		});
	}
}
