package br.com.south.service.impl;

import br.com.south.h2.entity.Item;
import br.com.south.h2.repository.ItemRepository;
import br.com.south.service.ItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ItemServiceImpl implements ItemService {

	private final ItemRepository repository;

	@Autowired
	public ItemServiceImpl(ItemRepository repository) {
		this.repository = repository;
	}

	@Override
	public Item saveOrUpdate(Item item) {
		log.info("Salvando item.");
		return repository.save(item);
	}

	@Override
	public List<Item> findAll() {
		log.info("Buscando todos os itens.");
		return repository.findAll();
	}
}
