package br.com.south.service.impl;

import br.com.south.h2.entity.Item;
import br.com.south.h2.entity.Sale;
import br.com.south.h2.repository.SaleRepository;
import br.com.south.service.ItemService;
import br.com.south.service.SaleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

@Slf4j
@Service
public class SaleServiceImpl implements SaleService {

	private final SaleRepository repository;
	private final ItemService itemService;

	@Autowired
	public SaleServiceImpl(SaleRepository repository, ItemService itemService) {
		this.repository = repository;
		this.itemService = itemService;
	}

	@Override
	public Sale saveOrUpdate(Sale sale) {
		log.info("Salvando venda.");
		return repository.save(sale);
	}

	@Override
	public String mostExpensiveSaleId() {
		log.info("Obtendo venda mais cara");
		List<Item> itens = itemService.findAll();
		Item item = itens.stream().max(Comparator.comparing(Item::getPrice)).orElseThrow(NoSuchElementException::new);
		return Arrays.toString(item.getSales().stream().map(Sale::getSaleId).toArray()).replace("[", "").replace("]", "");
	}

	@Override
	public List<Sale> findAll() {
		log.info("Buscando todas as vendas");
		return repository.findAll();
	}
}
