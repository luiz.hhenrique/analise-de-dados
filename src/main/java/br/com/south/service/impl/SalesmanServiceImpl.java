package br.com.south.service.impl;

import br.com.south.h2.entity.Sale;
import br.com.south.h2.entity.Salesman;
import br.com.south.h2.repository.SalesmanRepository;
import br.com.south.service.SaleService;
import br.com.south.service.SalesmanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

@Slf4j
@Service
public class SalesmanServiceImpl implements SalesmanService {

	private final SalesmanRepository repository;
	private final SaleService saleService;

	@Autowired
	public SalesmanServiceImpl(SalesmanRepository repository, SaleService saleService) {
		this.repository = repository;
		this.saleService = saleService;
	}

	@Override
	public Salesman saveOrUpdate(Salesman salesman) {
		log.info("Salvando vendedor.");
		return repository.save(salesman);
	}

	@Override
	public int totalSalesman() {
		log.info("Obtendo total de vendedor");
		return this.findAll().size();
	}

	@Override
	public String worstSalesman() {
		log.info("Obtendo pior vendedor");
		List<Sale> sales = saleService.findAll();
		Sale sale = sales.stream().min(Comparator.comparing(Sale::getSalesman)).orElseThrow(NoSuchElementException::new);
		return sale.getSalesman();
	}

	@Override
	public List<Salesman> findAll() {
		return repository.findAll();
	}
}
