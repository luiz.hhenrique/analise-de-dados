package br.com.south.service.impl;

import br.com.south.service.CustomerService;
import br.com.south.service.ExporterService;
import br.com.south.service.SaleService;
import br.com.south.service.SalesmanService;
import br.com.south.shared.File;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ExporterServiceImpl extends File implements ExporterService {

	private final SaleService saleService;
	private final SalesmanService salesmanService;
	private final CustomerService customerService;

	@Autowired
	public ExporterServiceImpl(SaleService saleService, SalesmanService salesmanService, CustomerService customerService) {
		this.saleService = saleService;
		this.salesmanService = salesmanService;
		this.customerService = customerService;
	}

	public String mountReport() {
		StringBuilder content = new StringBuilder();

		int totalSalesman = salesmanService.totalSalesman();
		int totalCustomer = customerService.totalCustomer();
		String mostExpensiveSaleId = saleService.mostExpensiveSaleId();
		String worstSalesman = salesmanService.worstSalesman();

		content.append(totalSalesman).append("ç").append(totalCustomer).append("ç").append(mostExpensiveSaleId).append("ç").append(worstSalesman);
		return content.toString();
	}

	public void exportReport(String content) {
		log.info("Escrevendo relatório de importação.");
		var path = this.getFileByHomePath("/data/out");

		this.writeFile(path.getPath(), "report", content.getBytes());
	}
}
