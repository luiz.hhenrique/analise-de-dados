package br.com.south.service;

import br.com.south.h2.entity.Customer;

import javax.transaction.Transactional;
import java.util.List;

public interface CustomerService {

	@Transactional(rollbackOn = Exception.class)
	Customer saveOrUpdate(Customer customer);

	int totalCustomer();

	List<Customer> findAll();
}
