package br.com.south.service;

import br.com.south.h2.entity.Sale;

import javax.transaction.Transactional;
import java.util.List;

public interface SaleService {

	@Transactional(rollbackOn = Exception.class)
	Sale saveOrUpdate(Sale sale);

	String mostExpensiveSaleId();

	List<Sale> findAll();
}
