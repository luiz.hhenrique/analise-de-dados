package br.com.south.service;

public interface ExporterService {

	String mountReport();

	void exportReport(String content);
}
