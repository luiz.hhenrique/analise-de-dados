package br.com.south;

import br.com.south.service.ImporterService;
import br.com.south.shared.WatchFolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class AnaliseDeDadosApplication {

	@Autowired private WatchFolder watchFolder;
	@Autowired private ImporterService importerService;

	public static void main(String[] args) {
		SpringApplication.run(AnaliseDeDadosApplication.class, args);
	}

	@PostConstruct
	public void init() {
		this.watchFolder.watchFolderIN();
		this.importerService.importArchiveDataInit();
	}

}
