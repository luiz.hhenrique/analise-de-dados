package br.com.south.service.impl;

import br.com.south.h2.entity.Item;
import br.com.south.h2.repository.ItemRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

class ItemServiceImplTest {

	@InjectMocks ItemServiceImpl itemService;
	@Mock ItemRepository itemRepository;

	private Item item;
	private List<Item> itemList;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);

		item = new Item();
		item.setId(1L);
		item.setSales(new HashSet<>(0));
		item.setItemId("123");
		item.setPrice(12.0);
		item.setAmount(1L);

		itemList = new ArrayList<>();
		itemList.add(item);
	}

	@Test
	void saveOrUpdate() {

		// Dado que
		Mockito.when(itemRepository.save(Mockito.any(Item.class))).thenReturn(this.item);

		// Quando
		Item item = itemService.saveOrUpdate(this.item);

		// Então
		Assertions.assertEquals(this.item, item);
	}

	@Test
	void findAll() {

		// Dado que
		Mockito.when(itemRepository.findAll()).thenReturn(this.itemList);

		// Quando
		List<Item> itens = itemService.findAll();

		// Então
		Assertions.assertEquals(this.itemList, itens);
	}
}
