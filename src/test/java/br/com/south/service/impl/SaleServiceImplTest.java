package br.com.south.service.impl;

import br.com.south.h2.entity.Item;
import br.com.south.h2.entity.Sale;
import br.com.south.h2.repository.SaleRepository;
import br.com.south.service.ItemService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class SaleServiceImplTest {

	@InjectMocks SaleServiceImpl saleService;
	@Mock SaleRepository saleRepository;
	@Mock ItemService itemService;

	private Sale sale;
	private Set<Sale> saleSet;

	private List<Item> itemList;
	private List<Sale> saleList;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);

		sale = new Sale();
		sale.setId(1L);
		sale.setSalesman("Unit Test");
		sale.setSaleId("123");

		saleSet = new HashSet<>(0);
		saleSet.add(sale);

		itemList = new ArrayList<>();
		for (int i = 0; i < 10 ; i++) {
			itemList = configPreCondiditionOnItem(12.0 * i, i);
		}

		saleList = new ArrayList<>();
		saleList.add(sale);
	}

	@Test
	void saveOrUpdate() {

		// Dado que
		Mockito.when(saleRepository.save(Mockito.any(Sale.class))).thenReturn(this.sale);

		// Quando
		Sale sale = saleService.saveOrUpdate(this.sale);

		// Então
		Assertions.assertEquals(this.sale, sale);
	}

	@Test
	void mostExpensiveSaleId() {

		// Dado que
		Mockito.when(itemService.findAll()).thenReturn(this.itemList);

		// Quando
		String mostExpensiveSaleId = saleService.mostExpensiveSaleId();

		// Então
		Assertions.assertEquals(this.sale.getSaleId(), mostExpensiveSaleId);
	}

	@Test
	void findAll() {

		// Dado que
		Mockito.when(saleRepository.findAll()).thenReturn(this.saleList);

		// Quando
		List<Sale> sales = saleService.findAll();

		// Então
		Assertions.assertEquals(this.saleList, sales);
	}

	private List<Item> configPreCondiditionOnItem(Double price, long amount) {

		List<Item> itens = new ArrayList<>();

		Item item = new Item();
		item.setId(1L);
		item.setPrice(price);
		item.setAmount(amount);
		item.setSales(this.saleSet);

		sale.setItem(item);
		itens.add(item);

		return itens;
	}
}
