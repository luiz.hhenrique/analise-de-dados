package br.com.south.service.impl;

import br.com.south.h2.entity.Customer;
import br.com.south.h2.entity.Item;
import br.com.south.h2.entity.Sale;
import br.com.south.h2.entity.Salesman;
import br.com.south.h2.repository.CustomerRepository;
import br.com.south.h2.repository.SaleRepository;
import br.com.south.h2.repository.SalesmanRepository;
import br.com.south.service.CustomerService;
import br.com.south.service.ItemService;
import br.com.south.service.SaleService;
import br.com.south.service.SalesmanService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

class ExporterServiceImplTest {

	@InjectMocks ExporterServiceImpl exporterService;

	@Mock SaleService saleService;
	@Mock SalesmanService salesmanService;
	@Mock CustomerService customerService;
	@Mock ItemService itemService;

	private Sale sale;
	private Salesman salesman;

	private List<Salesman> salesmanList;
	private List<Sale> saleList;
	private List<Customer> customerList;
	private List<Item> itemList;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);

		salesman = new Salesman();
		salesman.setDocument("12345678910");
		salesman.setName("Unit Test Salesman");
		salesman.setSalary(12.0);

		salesmanList = new ArrayList<>();
		salesmanList.add(salesman);

		Item item = new Item();
		item.setId(1L);
		item.setItemId("123");
		item.setAmount(1L);
		item.setPrice(12.0);
		item.setSales(new HashSet<>(0));

		itemList = new ArrayList<>();
		itemList.add(item);

		sale = new Sale();
		sale.setId(1L);
		sale.setSaleId("123");
		sale.setItem(item);
		sale.setSalesman(salesman.getName());

		saleList = new ArrayList<>();
		saleList.add(sale);

		Customer customer = new Customer();
		customer.setId(1L);
		customer.setName("Unit Test Customer");
		customer.setDocument("12345678910");
		customer.setBusinessArea("JUnit Test");

		customerList = new ArrayList<>();
		customerList.add(customer);
	}

	@Test
	void mountReport() {

		// Dado que
		Mockito.when(salesmanService.findAll()).thenReturn(this.salesmanList);
		Mockito.when(customerService.findAll()).thenReturn(this.customerList);
		Mockito.when(itemService.findAll()).thenReturn(this.itemList);
		Mockito.when(saleService.findAll()).thenReturn(this.saleList);

		int totalSalesman = salesmanService.totalSalesman();
		int totalCustomer = customerService.totalCustomer();
		String mostExpensiveSaleId = saleService.mostExpensiveSaleId();
		String worstSalesman = salesmanService.worstSalesman();

		String content = configPreCondititionOnExporter(totalSalesman, totalCustomer, mostExpensiveSaleId, worstSalesman);

		// Quando
		String contentExporter = exporterService.mountReport();

		// Então
		Assertions.assertEquals(content, contentExporter);
	}

	private String configPreCondititionOnExporter(int totalSalesman, int totalCustomer, String mostExpensiveSaleId, String worstSalesman) {
		StringBuilder content = new StringBuilder();

		content.append(totalSalesman).append("ç").append(totalCustomer).append("ç").append(mostExpensiveSaleId).append("ç").append(worstSalesman);
		return content.toString();
	}
}
