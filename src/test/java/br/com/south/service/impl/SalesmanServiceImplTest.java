package br.com.south.service.impl;

import br.com.south.h2.entity.Item;
import br.com.south.h2.entity.Sale;
import br.com.south.h2.entity.Salesman;
import br.com.south.h2.repository.SalesmanRepository;
import br.com.south.service.SaleService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

class SalesmanServiceImplTest {

	@InjectMocks SalesmanServiceImpl salesmanService;
	@Mock SalesmanRepository salesmanRepository;
	@Mock SaleService saleService;

	private Salesman salesman;
	private List<Salesman> salesmanList;

	private Sale sale;
	private List<Sale> saleList;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
		salesman = new Salesman();
		salesman.setId(1L);
		salesman.setSalary(12.0);
		salesman.setName("Unit Test");
		salesman.setDocument("12345678910");

		salesmanList = new ArrayList<>();
		salesmanList.add(salesman);

		sale = new Sale();
		sale.setId(1L);
		sale.setItem(new Item());
		sale.setSaleId("123");
		sale.setSalesman(salesman.getName());

		saleList = new ArrayList<>();
		saleList.add(sale);
	}

	@Test
	void saveOrUpdate() {

		// Dado que
		Mockito.when(salesmanRepository.save(Mockito.any(Salesman.class))).thenReturn(this.salesman);

		// Quando
		Salesman salesman = salesmanService.saveOrUpdate(this.salesman);

		// Então
		Assertions.assertEquals(this.salesman, salesman);
	}

	@Test
	void totalSalesman() {

		// Dado que
		Mockito.when(salesmanRepository.findAll()).thenReturn(this.salesmanList);

		// Quando
		int total = salesmanService.totalSalesman();

		// Então
		Assertions.assertEquals(this.salesmanList.size(), total);
	}

	@Test
	void worstSalesman() {

		// Dado que
		Mockito.when(saleService.findAll()).thenReturn(this.saleList);

		// Quando
		String worstSalesman = salesmanService.worstSalesman();

		// Então
		Assertions.assertEquals(this.salesman.getName(), worstSalesman);
	}
}
