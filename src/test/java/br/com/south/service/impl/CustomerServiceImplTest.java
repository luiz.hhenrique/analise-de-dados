package br.com.south.service.impl;

import br.com.south.h2.entity.Customer;
import br.com.south.h2.repository.CustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CustomerServiceImplTest {

	@InjectMocks private CustomerServiceImpl customerService;
	@Mock private CustomerRepository customerRepository;

	private Customer customer;
	private List<Customer> customerList;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);

		customer = new Customer();
		customer.setId(1L);
		customer.setBusinessArea("Test");
		customer.setName("Unit Test");
		customer.setDocument("12345678910");

		customerList = new ArrayList<>();
		customerList.add(customer);
	}

	@Test
	void saveOrUpdate() {

		// dado que
		Mockito.when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(this.customer);

		// quando
		Customer customer = customerService.saveOrUpdate(this.customer);

		// então
		Assertions.assertEquals(this.customer, customer);
	}

	@Test
	void totalCustomer() {

		// dado que
		Mockito.when(customerRepository.findAll()).thenReturn(this.customerList);

		// quando
		int totalCustomer = customerService.totalCustomer();

		// então
		Assertions.assertEquals(this.customerList.size(), totalCustomer);

	}
}
